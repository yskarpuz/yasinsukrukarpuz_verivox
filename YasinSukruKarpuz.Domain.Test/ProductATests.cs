﻿using System;
using NUnit.Framework;
using System.Collections;
using YasinSukruKarpuz.Domain.Models;
using YasinSukruKarpuz.Domain.ValueObjects;

namespace YasinSukruKarpuz.Domain.Test
{
    [TestFixture]
    public class ProductATests
    {
        [TestCaseSource(typeof(ProductATestCases), "Cases")]
        public int Calculate_Annual_Cost_By_Consumption(int consumption)
        {
            return new ProductA(new Consumption(consumption)).AnnualCost.Value;
        }
    }

    public class ProductATestCases
    {
        public static IEnumerable Cases
        {
            get
            {
                yield return new TestCaseData(3500).Returns(830);
                yield return new TestCaseData(4500).Returns(1050);
                yield return new TestCaseData(6000).Returns(1380);
            }
        }
    }
}
