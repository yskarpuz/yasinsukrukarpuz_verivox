﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YasinSukruKarpuz.Domain.ValueObjects
{
    public class Consumption : UnitBase
    {
        public Consumption(int value) : base(value, "kWh/year")
        { }

        public static Consumption operator -(Consumption c, Consumption operand)
        {
            return new Consumption(c.Value - operand.Value);
        }
        public static Consumption operator +(Consumption c, Consumption operand)
        {
            return new Consumption(c.Value + operand.Value);
        } 
        public static bool operator <(Consumption first, Consumption second)
        {
            return (first.Value < second.Value);
        } 
        public static bool operator >(Consumption first, Consumption second)
        {
            return (first.Value > second.Value);
        }
    }

}
