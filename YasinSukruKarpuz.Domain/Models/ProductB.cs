﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YasinSukruKarpuz.Domain.ValueObjects;

namespace YasinSukruKarpuz.Domain.Models
{
    public class ProductB : ProductBase
    {
        public const int PACKAGE_DEFAULT_COST = 800;
        public readonly Consumption Limit = new Consumption(4000);

        public ProductB(Consumption consumption) : base("Packaged tariff", new ConsumptionCost(30), consumption)
        { }

        public override AnnualCost AnnualCost
        {
            get
            {
                int result = 0;
                if (this.Consumption < this.Limit)
                    result = PACKAGE_DEFAULT_COST;
                else
                {
                    result = PACKAGE_DEFAULT_COST + ((this.Consumption - this.Limit).Value * this.ConsumptionCost.Value) / 100;
                } 
                return new AnnualCost(result);
            }
        }
    }
}
