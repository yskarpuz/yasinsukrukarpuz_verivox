﻿using System;
using NUnit.Framework;
using System.Collections;
using YasinSukruKarpuz.Domain.Models;
using YasinSukruKarpuz.Domain.ValueObjects;

namespace YasinSukruKarpuz.Domain.Test
{
    [TestFixture]
    public class ProductBTests
    {
        [TestCaseSource(typeof(ProductBTestCases), "Cases")]
        public int Calculate_Annual_Cost_By_Consumption(int consumption)
        {
            return new ProductB(new Consumption(consumption)).AnnualCost.Value;
        }
    }

    public class ProductBTestCases
    {
        public static IEnumerable Cases
        {
            get
            {
                yield return new TestCaseData(3500).Returns(800);
                yield return new TestCaseData(4500).Returns(950);
                yield return new TestCaseData(6000).Returns(1400);
            }
        }
    }
}
