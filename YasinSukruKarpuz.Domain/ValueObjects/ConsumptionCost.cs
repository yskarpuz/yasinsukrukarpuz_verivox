﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YasinSukruKarpuz.Domain.ValueObjects
{
    public class ConsumptionCost : UnitBase
    {
        public ConsumptionCost(int value) : base(value, "cent/kWh")
        { }

        public static bool operator <(ConsumptionCost first, ConsumptionCost second)
        {
            return (first.Value < second.Value);
        } 
        public static bool operator >(ConsumptionCost first, ConsumptionCost second)
        {
            return (first.Value > second.Value);
        }
    }
}
