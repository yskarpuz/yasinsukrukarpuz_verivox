﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YasinSukruKarpuz.Domain.ValueObjects
{
    public class AnnualCost : UnitBase
    {
        public AnnualCost(int value) : base(value, "€/year")
        { }
    }
}
