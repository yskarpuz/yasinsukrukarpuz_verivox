﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YasinSukruKarpuz.Domain.ValueObjects
{
    public class UnitBase
    {
        protected UnitBase(int value, string unit)
        {
            this.value = value;
            this.unit = unit;
        }

        private int value;
        public int Value
        {
            get { return value; }
        }
        private string unit;
        public string Unit { get { return unit; } }
         
        public static UnitBase operator *(UnitBase c, int operand)
        {
            return new UnitBase(c.Value * operand, c.Unit);
        }
        public static UnitBase operator +(UnitBase c, int operand)
        {
            return new UnitBase(c.Value + operand, c.Unit);
        }
         
        public override bool Equals(object o)
        {
            return ((UnitBase)o).Value == this.Value && ((UnitBase)o).Unit == this.Unit; 
        }
        public override int GetHashCode()
        {
            return this.ToString().GetHashCode();
        }
         
    }
}
