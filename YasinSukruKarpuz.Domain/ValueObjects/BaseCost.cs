﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YasinSukruKarpuz.Domain.ValueObjects
{
    public class BaseCost : UnitBase
    {
        public BaseCost(int value) : base(value, "€")
        { }
    }
}
