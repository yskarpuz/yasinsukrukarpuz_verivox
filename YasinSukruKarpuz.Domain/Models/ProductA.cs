﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YasinSukruKarpuz.Domain.ValueObjects;

namespace YasinSukruKarpuz.Domain.Models
{
    public class ProductA : ProductBase
    {
        public readonly BaseCost BaseCost = new BaseCost(5);
        public ProductA(Consumption consumption) : base("Basic electricity tariff", new ConsumptionCost(22), consumption)
        { }
          
        public override AnnualCost AnnualCost
        {
            get
            {
                var result = (this.BaseCost * 12).Value + (Consumption.Value * ConsumptionCost.Value) / 100;
                return new AnnualCost(result);
            }
        }
    }
}
