﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YasinSukruKarpuz.Domain.ValueObjects;

namespace YasinSukruKarpuz.Domain.Models
{
    public abstract class ProductBase
    { 
        private ConsumptionCost consumptionCost;
        private Consumption consumption;

        public ProductBase(string tariffName, ConsumptionCost consumptionCost, Consumption consumption)
        {
            this.TariffName = tariffName;
            this.Consumption = consumption;
            this.ConsumptionCost = consumptionCost; 
        } 

        public ConsumptionCost ConsumptionCost { get; private set; }
        public string TariffName { get; private set; }
        public abstract AnnualCost AnnualCost { get; }
        public Consumption Consumption { get; private set; } 
    }
}
